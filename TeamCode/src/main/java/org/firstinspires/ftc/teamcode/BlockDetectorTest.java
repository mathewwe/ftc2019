package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.corningrobotics.enderbots.endercv.CameraViewDisplay;

@Autonomous(name = "Block Detector Test")
public class BlockDetectorTest extends LinearOpMode {
    BlockDetector openCV;
    double start;
    @Override
    public void runOpMode(){
        openCV = new BlockDetector();
        // can replace with ActivityViewDisplay.getInstance() for fullscreen
        openCV.init(hardwareMap.appContext, CameraViewDisplay.getInstance());

        waitForStart();
        openCV.enable();
        start = System.currentTimeMillis();
        while (opModeIsActive()){
            telemetry.addData("x", openCV.getBlobX());
            telemetry.addData("y", openCV.getBlobY());
            telemetry.update();
        }
        openCV.disable();
    }
}
