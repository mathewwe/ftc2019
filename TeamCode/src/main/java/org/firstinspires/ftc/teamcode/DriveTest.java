package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import org.robocavs.lib.hardware.BoschIMU;
import org.robocavs.lib.hardware.Drivetrain;
import org.robocavs.lib.hardware.MecanumDrivetrain;
import org.robocavs.lib.hardware.Odometer;
import org.robocavs.lib.motion.GenericController;
import org.robocavs.lib.motion.MotionCommandsOdometer;

@TeleOp(name = "Drive Method Test")
public class DriveTest extends LinearOpMode {
    Drivetrain dt = new MecanumDrivetrain(10/2.54, 1.3333, Drivetrain.MotorType.N40);
    MotionCommandsOdometer motion = new MotionCommandsOdometer(this, dt, MotionCommandsOdometer.DrivetrainType.MECANUM);
    BoschIMU imu = new BoschIMU();
    GenericController PID = new GenericController();
    GenericController PID1 = new GenericController();
    GenericController PID2 = new GenericController();
    @Override
    public void runOpMode() throws InterruptedException {
        motion.init();
        imu.init(hardwareMap, "imu");
        waitForStart();
        imu.start();

        dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
        motion.odometer.reset(Odometer.Axis.ALL);
        double heading = 0;
        double maxPower = .75;
        PID.resetController();
        PID1.resetController();
        PID2.resetController();
        while (opModeIsActive()) {
            double x = gamepad1.left_stick_x*35;
            double y = -gamepad1.left_stick_y*35;
            imu.loop();
            double drivePower = PID.controllerOutput(motion.odometer.EncoderTicksPerIn()*y, motion.odometer.getCurrPosY(), maxPower, .0006, 0, 0);
            double turnPower = PID1.gyroControllerOutput(heading, imu.heading, 1, .0137, 0, 0);
            double strafePower = PID2.controllerOutput(motion.odometer.EncoderTicksPerIn()*x, motion.odometer.getCurrPosX(), maxPower, .0008, 0, 0);
            dt.FL.setPower(Range.clip(drivePower+strafePower-turnPower, -1, 1)); //+strafePower-turnPower
            dt.FR.setPower(Range.clip(drivePower-strafePower+turnPower, -1, 1)); //-strafePower+turnPower
            dt.RL.setPower(Range.clip(drivePower-strafePower-turnPower, -1, 1)); //-strafePower-turnPower
            dt.RR.setPower(Range.clip(drivePower+strafePower+turnPower, -1, 1)); //+strafePower+turnPower

            telemetry.addData("CurrPosY", motion.odometer.getDistanceY());
            telemetry.addData("CurrPosX", motion.odometer.getDistanceX());
            telemetry.addData("Heading", imu.heading);
            telemetry.update();
        }

    }
}
