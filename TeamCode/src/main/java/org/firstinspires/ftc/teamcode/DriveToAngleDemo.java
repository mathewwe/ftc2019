package org.firstinspires.ftc.teamcode;


import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import net.frogbots.ftcopmodetunercommon.opmode.TunableLinearOpMode;

import org.robocavs.lib.hardware.BoschIMU;
import org.robocavs.lib.hardware.MecanumDrivetrain;
import org.robocavs.lib.motion.EncoderMotionProfile;
import org.robocavs.lib.motion.GenericController;

@Autonomous(name = "Drive To Angle Demo")
public class DriveToAngleDemo extends TunableLinearOpMode {
    EncoderMotionProfile MP = new EncoderMotionProfile();
    MecanumDrivetrain dt = new MecanumDrivetrain(10/2.54, 1.333, MecanumDrivetrain.MotorType.N40);
    BoschIMU imu = new BoschIMU();
    GenericController PID = new GenericController();
    @Override
    public void runOpMode(){
        dt.init(hardwareMap);
        imu.init(hardwareMap, "imu");
        dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
        waitForStart();
        imu.start();
        PID.resetController();
        while (opModeIsActive()){
            int rampingTicks = getInt("Ramp-Up Ticks");
            double targetPos = getDouble("Target Position");
            double maxPower = getDouble("Max Power");
            double startPower = getDouble("Starting Power");
            double endPower = getDouble("Ending Power");
            double turnKP = getDouble("Turning kP");
            double setPoint = getDouble("Angle Setpoint");
            imu.loop();
            double power = MP.trapezoidalMP(startPower, endPower, maxPower , dt.FL.getCurrentPosition(), (int)(targetPos*dt.EncoderTicksPerIn()), rampingTicks);
            double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, turnKP, 0, 0);
            dt.FL.setPower(Range.clip(maxPower*Math.cos(setPoint*(Math.PI/180)-(Math.PI/4)) - turnPower, -1, 1)); //drives right on chris' chassis
            dt.FR.setPower(Range.clip(maxPower*Math.sin(setPoint*(Math.PI/180)-(Math.PI/4)) + turnPower, -1, 1));
            dt.RL.setPower(Range.clip(maxPower*Math.sin(setPoint*(Math.PI/180)-(Math.PI/4)) - turnPower, -1, 1));
            dt.RR.setPower(Range.clip(maxPower*Math.cos(setPoint*(Math.PI/180)-(Math.PI/4)) + turnPower, -1, 1));
            telemetry.addData("Power", power);
            telemetry.addData("Current Position", dt.FL.getCurrentPosition());
            telemetry.addData("Target Position", targetPos*dt.EncoderTicksPerIn());
            telemetry.addData("Heading", imu.heading);
            telemetry.update();
        }
    }
}
