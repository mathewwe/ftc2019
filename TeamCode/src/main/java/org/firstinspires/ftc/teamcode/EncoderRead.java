package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.robocavs.lib.hardware.Drivetrain;
import org.robocavs.lib.hardware.MecanumDrivetrain;
import org.robocavs.lib.hardware.Odometer;

@TeleOp(name = "Encoder Read")
public class EncoderRead extends LinearOpMode {
    Drivetrain dt = new MecanumDrivetrain(5.95/2.54, 1, Drivetrain.MotorType.E4T);
    Odometer odometer = new Odometer(6/2.54, 1440);
    @Override
    public void runOpMode() throws InterruptedException {
        dt.init(hardwareMap);
        odometer.init(hardwareMap);
        dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        waitForStart();
        dt.setMotorModes(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        while (opModeIsActive()){
            telemetry.addData("Encoder Distance (Forward)", odometer.getCurrPosY());
            telemetry.addData("Encoder Distance (Sideways)", odometer.getCurrPosX());
            telemetry.update();
        }
    }
}
