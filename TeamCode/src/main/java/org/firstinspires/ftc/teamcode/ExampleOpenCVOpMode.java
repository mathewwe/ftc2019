package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.corningrobotics.enderbots.endercv.CameraViewDisplay;

/**
 * Created by guinea on 10/5/17.
 * This is a sample opmode that demonstrates the use of an OpenCVPipeline with FTC code.
 * When the x button is pressed on controller one, the camera is set to show areas of the image
 * where a certain color is, in this case, blue.
 *
 */
@TeleOp(name="Example: Blue Vision Demo")
@Disabled
public class ExampleOpenCVOpMode extends OpMode {
    ExamplePipeline openCVPipeline;
    @Override
    public void init() {
        openCVPipeline = new ExamplePipeline();
        // can replace with ActivityViewDisplay.getInstance() for fullscreen
        openCVPipeline.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        openCVPipeline.setShowBlue(false);
        // start the vision system
        openCVPipeline.enable();
    }

    @Override
    public void loop() {
        openCVPipeline.setShowBlue(true);
    }

    public void stop() {
        // stop the vision system
        openCVPipeline.disable();
    }
}