package org.firstinspires.ftc.teamcode;


import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;

import net.frogbots.ftcopmodetunercommon.opmode.TunableLinearOpMode;

import org.robocavs.lib.hardware.MecanumDrivetrain;
import org.robocavs.lib.motion.EncoderMotionProfile;

@Autonomous(name = "Motion Profile Testing")
public class MotionProfileTest extends TunableLinearOpMode {
    EncoderMotionProfile MP = new EncoderMotionProfile();
    MecanumDrivetrain dt = new MecanumDrivetrain(10/2.54, 1.333, MecanumDrivetrain.MotorType.N40);
    @Override
    public void runOpMode(){
        dt.init(hardwareMap);
        dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
        waitForStart();
        while (opModeIsActive()){
            int rampingTicks = getInt("Ramp-Up Ticks");
            double targetPos = getDouble("Target Position");
            double maxPower = getDouble("Max Power");
            double startPower = getDouble("Starting Power");
            double endPower = getDouble("Ending Power");
            double power = MP.trapezoidalMP(startPower, endPower, maxPower , dt.FL.getCurrentPosition(), (int)(targetPos*dt.EncoderTicksPerIn()), rampingTicks);
            dt.FL.setPower(power);
            dt.FR.setPower(power);
            dt.RL.setPower(power);
            dt.RR.setPower(power);
            telemetry.addData("Power", power);
            telemetry.addData("Current Position", dt.FL.getCurrentPosition());
            telemetry.addData("Target Position", targetPos*dt.EncoderTicksPerIn());
            telemetry.update();
        }
    }
}
