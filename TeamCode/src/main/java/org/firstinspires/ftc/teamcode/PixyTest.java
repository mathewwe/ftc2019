package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.robocavs.lib.vision.Pixy2;

public class PixyTest extends OpMode {
    Pixy2 pixy2;
    byte[] VersionRequest = {(byte) 0xae, (byte) 0xc1, (byte)0x0e, (byte)0x00};
    @Override
    public void init(){
        pixy2 = hardwareMap.get(Pixy2.class, "pixy2");
    }
    @Override
    public void loop(){
        pixy2.write(VersionRequest);
        byte[] a = pixy2.read();
        telemetry.addData("Data", a[0]);
        telemetry.addData("Data", a[1]);
        telemetry.addData("Data", a[2]);
        telemetry.addData("Data", a[3]);
        telemetry.addData("Data", a[4]);
        telemetry.addData("Data", a[5]);
        telemetry.addData("Data", a[6]);
        telemetry.addData("Data", a[7]);
        telemetry.addData("Data", a[8]);
        telemetry.addData("Data", a[9]);
        telemetry.addData("Data", a[10]);
        telemetry.addData("Data", a[11]);
        telemetry.addData("Data", a[12]);
        telemetry.addData("Data", a[13]);
        telemetry.addData("Data", a[14]);
        telemetry.addData("Data", a[15]);
        telemetry.addData("Data", a[16]);
        telemetry.addData("Data", a[17]);
        telemetry.addData("Data", a[18]);
        telemetry.addData("Data", a[19]);
        telemetry.addData("Data", a[20]);
        telemetry.addData("Data", a[21]);
        telemetry.addData("Data", a[22]);

    }
}
