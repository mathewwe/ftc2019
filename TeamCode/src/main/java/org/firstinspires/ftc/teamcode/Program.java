package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

public class Program extends LinearOpMode {
    DcMotor left;
    DcMotor right;
    @Override
    public void runOpMode() throws InterruptedException {
        left = hardwareMap.dcMotor.get("left");
        right= hardwareMap. dcMotor.get ("right");
        waitForStart();

        //left.setPower(1);
        //right.setPower(1);
        //sleep(5000);

        while(opModeIsActive()){
            left.setPower(gamepad1.left_stick_y);
            right.setPower(gamepad1.right_stick_y);
            if(gamepad1.a){
                break;
            }
        }

    }
}
