package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

import org.robocavs.lib.hardware.BoschIMU;
import org.robocavs.lib.hardware.Drivetrain;
import org.robocavs.lib.hardware.MecanumDrivetrain;
import org.robocavs.lib.hardware.Odometer;

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name = "TeleOp w/ Encoders")
public class TeleOp extends OpMode {
    Drivetrain dt = new MecanumDrivetrain(10/2.54, 1.3333, Drivetrain.MotorType.N40);
    DcMotor extend;
    Servo fast;
    Odometer odometer = new Odometer(6/2.54, 1440);
    BoschIMU imu = new BoschIMU();
    @Override
    public void init() {
        dt.init(hardwareMap);
        extend = hardwareMap.dcMotor.get("extend");
        fast = hardwareMap.servo.get("fast");
        extend.setDirection(DcMotorSimple.Direction.REVERSE);
        extend.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        extend.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        odometer.init(hardwareMap);
        odometer.y.setDirection(DcMotorSimple.Direction.REVERSE);
        imu.init(hardwareMap, "imu");
        dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }
    @Override
    public void start(){
        odometer.reset(Odometer.Axis.ALL);
        imu.start();
        dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
    }
    @Override
    public void loop() {
        imu.loop();
        double r = Math.pow(Math.hypot(gamepad1.left_stick_x, -gamepad1.left_stick_y), 3);
        double robotAngle = Math.atan2(-gamepad1.left_stick_y, gamepad1.left_stick_x) - Math.PI / 4;
        double rightX = gamepad1.right_stick_x;
        final double v1 = r * Math.cos(robotAngle) - rightX;
        final double v2 = r * Math.sin(robotAngle) + rightX;
        final double v3 = r * Math.sin(robotAngle) - rightX;
        final double v4 = r * Math.cos(robotAngle) + rightX;

        dt.FL.setPower(v1);
        dt.FR.setPower(v2);
        dt.RL.setPower(v3);
        dt.RR.setPower(v4);

        if (gamepad1.a){
            odometer.reset(Odometer.Axis.ALL);
            imu.start();
        }
        odometer.x.setPower(gamepad2.right_trigger-gamepad2.left_trigger);
        odometer.y.setPower(gamepad2.right_trigger-gamepad2.left_trigger);
        extend.setTargetPosition(0);
        extend.setPower(1);
        fast.setPosition(Math.abs(gamepad2.left_stick_x*.3));

        telemetry.addData("X Distance", odometer.getDistanceX());
        telemetry.addData("Y Distance", odometer.getDistanceY());
        telemetry.addData("X Mode", odometer.x.getMode());
        telemetry.addData("Y Mode", odometer.y.getMode());
        telemetry.addData("Heading", imu.heading);
        telemetry.addData("X Position", odometer.getCurrPosX());
        telemetry.addData("Y Position", odometer.getCurrPosY());
        telemetry.addData("Extend Position", extend.getCurrentPosition());
        telemetry.addData("Servo Pos", Math.abs(gamepad2.left_stick_x*.3));

    }
}
