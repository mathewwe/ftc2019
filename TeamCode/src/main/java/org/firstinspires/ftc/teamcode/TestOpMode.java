package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;

import net.frogbots.ftcopmodetunercommon.opmode.TunableLinearOpMode;

import org.robocavs.lib.hardware.BoschIMU;
import org.robocavs.lib.hardware.Drivetrain;
import org.robocavs.lib.hardware.MecanumDrivetrain;
import org.robocavs.lib.hardware.Odometer;
import org.robocavs.lib.motion.MotionCommandsOdometer;

@Autonomous(name = "Motion Commands TestOp")
public class TestOpMode extends TunableLinearOpMode {
    Drivetrain dt = new MecanumDrivetrain(10/2.54, 1.3333, Drivetrain.MotorType.N40);
    MotionCommandsOdometer motion = new MotionCommandsOdometer(this, dt, MotionCommandsOdometer.DrivetrainType.MECANUM);
    BoschIMU imu = new BoschIMU();
    @Override
    public void runOpMode() throws InterruptedException {
        motion.init();
        imu.init(hardwareMap, "imu");
        waitForStart();
        imu.start();

        dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
        motion.odometer.reset(Odometer.Axis.ALL);
        motion.turnToHeading(1, 90, .5, imu);

    }
}
