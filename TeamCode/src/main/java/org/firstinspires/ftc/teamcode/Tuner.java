package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import net.frogbots.ftcopmodetunercommon.opmode.TunableLinearOpMode;

@TeleOp(name = "Tuner")
public class Tuner extends TunableLinearOpMode{
    @Override
    public void runOpMode() throws InterruptedException {
        waitForStart();
        while (opModeIsActive()){
            int i = getInt("i");
            telemetry.addData("i", i);
            telemetry.update();
        }
    }
}
