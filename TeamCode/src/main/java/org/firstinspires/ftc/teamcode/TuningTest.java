package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;

import net.frogbots.ftcopmodetunercommon.opmode.TunableLinearOpMode;

import org.robocavs.lib.hardware.BoschIMU;
import org.robocavs.lib.hardware.MecanumDrivetrain;
import org.robocavs.lib.motion.GenericController;

@Autonomous(name = "PID Tuner")
public class TuningTest extends TunableLinearOpMode {
    GenericController PID = new GenericController();
    MecanumDrivetrain dt = new MecanumDrivetrain(10/2.54, 1.333, MecanumDrivetrain.MotorType.N40);
    BoschIMU imu = new BoschIMU();

    @Override
    public void runOpMode() throws InterruptedException {
        dt.init(hardwareMap);
        dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
        imu.init(hardwareMap, "imu");
        waitForStart();
        imu.start();
        PID.resetController();
        while (opModeIsActive()){
            double kP = getDouble("kP");
            double kI = getDouble("kI");
            double kD = getDouble("kD");
            double setPoint = getDouble("Gyro Setpoint");
            double powerLimit = getDouble("Power Limit");
            imu.loop();
            double pidOut = PID.gyroControllerOutput(setPoint, imu.heading, powerLimit, kP, kI, kD);
            dt.FL.setPower(-pidOut);
            dt.FR.setPower(pidOut);
            dt.RL.setPower(-pidOut);
            dt.RR.setPower(pidOut);

            telemetry.addData("IMU Heading", imu.heading);
            telemetry.addData("P", kP*PID.getProportional());
            telemetry.addData("I", kI*PID.getIntegral());
            telemetry.addData("D", kD*PID.getDerivative());
            telemetry.addData("Iteration Time", PID.getIterationTime());
            telemetry.addData("Power Output", pidOut);
            telemetry.update();
        }
    }
}
