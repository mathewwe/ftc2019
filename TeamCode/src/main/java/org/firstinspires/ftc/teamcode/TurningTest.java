package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import net.frogbots.ftcopmodetunercommon.opmode.TunableLinearOpMode;

import org.robocavs.lib.hardware.BoschIMU;
import org.robocavs.lib.hardware.MecanumDrivetrain;
import org.robocavs.lib.motion.GenericController;

@Autonomous(name = "Turning Test")
public class TurningTest extends TunableLinearOpMode {
    MecanumDrivetrain dt = new MecanumDrivetrain(10/2.54, 1.333, MecanumDrivetrain.MotorType.N40);
    GenericController PID = new GenericController();
    BoschIMU imu = new BoschIMU();
    @Override
    public void runOpMode() throws InterruptedException {
        dt.init(hardwareMap);
        imu.init(hardwareMap, "imu");
        dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
        waitForStart();
        imu.start();
        PID.resetController();
        while (opModeIsActive()){ // TODO: 7/27/2018 Gain Schedule kP for motors at max power vs. less than max.
            double val = getDouble("Motor Power");
            double turnKP = getDouble("Turning kP");
            double setPoint = getDouble("Angle Setpoint");
            imu.loop();
            double turnPower = PID.gyroControllerOutput(setPoint, imu.heading, 1, turnKP, 0, 0);
            dt.FL.setPower(Range.clip(val - turnPower, -1, 1));
            dt.FR.setPower(Range.clip(val + turnPower, -1, 1));
            dt.RL.setPower(Range.clip(val - turnPower, -1, 1));
            dt.RR.setPower(Range.clip(val + turnPower, -1, 1));
            telemetry.addData("FLPOS", dt.FL.getCurrentPosition());
            telemetry.addData("FRPOS", dt.FR.getCurrentPosition());
            telemetry.addData("RLPOS", dt.RL.getCurrentPosition());
            telemetry.addData("RRPOS", dt.RR.getCurrentPosition());
            telemetry.addData("Ticks/Inch", dt.EncoderTicksPerIn());
            telemetry.addData("Heading", imu.heading);
            telemetry.update();
        }
    }
}