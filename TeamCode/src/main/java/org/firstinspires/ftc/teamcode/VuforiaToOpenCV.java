package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.corningrobotics.enderbots.endercv.CameraViewDisplay;
import org.robocavs.lib.vision.VuforiaCV;

@Autonomous(name = "vuforiaCV Tests")
public class VuforiaToOpenCV extends LinearOpMode {
    CV openCV;
    VuforiaCV vuforiaCV = new VuforiaCV();
    double start;
    @Override
    public void runOpMode(){
        openCV = new CV();
        // can replace with ActivityViewDisplay.getInstance() for fullscreen
        openCV.init(hardwareMap.appContext, CameraViewDisplay.getInstance());
        vuforiaCV.init();

        waitForStart();
        vuforiaCV.start();
        sleep(5000);
        vuforiaCV.close();
        sleep(10);
        openCV.enable();
        start = System.currentTimeMillis();
        while (opModeIsActive()){
            telemetry.addData("CenterX", openCV.rotatedRect().center.x);
            telemetry.addData("CenterY", openCV.rotatedRect().center.y);
            telemetry.addData("Largest Contour Size", openCV.maxContourVal());
            telemetry.addData("Largest Contour Index", openCV.maxContourIndex());
            telemetry.update();
        }
        openCV.disable();
    }
}
