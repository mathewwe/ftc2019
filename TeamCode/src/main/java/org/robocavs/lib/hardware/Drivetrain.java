package org.robocavs.lib.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

public abstract class Drivetrain { //Make sure that this not being an interface doesn't mess the whole deal up

    public DcMotor FL, FR, RL, RR; //See if this works with tank drives
    public enum MotorType{ORBITAL_20, N20, N40, N60, TORQUENADO, E4T}
    public abstract double EncoderTicksPerIn();
    public abstract void setMotorModes(DcMotor.RunMode runMode);
    public abstract void setMotorPowers(double power);
    public abstract void init(HardwareMap hwMap);
}
