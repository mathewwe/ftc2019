package org.robocavs.lib.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

public class MecanumDrivetrain extends Drivetrain {

    //public DcMotor FL, FR, RL, RR;

    private MotorType motorType;
    private double WHEEL_SIZE;
    private double GEAR_RATIO;

    public MecanumDrivetrain(double wheelSizeIn, double gearRatio, MotorType motorType){
        this.motorType = motorType;
        GEAR_RATIO = gearRatio;
        WHEEL_SIZE = wheelSizeIn;
    }
    @Override
    public double EncoderTicksPerIn(){
        double output;
        switch (motorType){
            case ORBITAL_20:
                output = (537.6/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case N20:
                output = (560/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case N40:
                output = (1120/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case N60:
                output = (1680/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case TORQUENADO:
                output = (1440/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case E4T:
                output = (1440/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            default:
                output = 0;
                break;
        }
        return output;
    }
    @Override
    public void setMotorModes(DcMotor.RunMode runMode){
        FL.setMode(runMode);
        FR.setMode(runMode);
        RL.setMode(runMode);
        RR.setMode(runMode);
    }
    @Override
    public void setMotorPowers(double power){
        FL.setPower(power);
        FR.setPower(power);
        RL.setPower(power);
        RR.setPower(power);
    }
    @Override
    public void init(HardwareMap hwMap){
        FL = hwMap.dcMotor.get("FL");
        FR = hwMap.dcMotor.get("FR");
        RL = hwMap.dcMotor.get("RL");
        RR = hwMap.dcMotor.get("RR");

        FR.setDirection(DcMotorSimple.Direction.REVERSE);
        RR.setDirection(DcMotorSimple.Direction.REVERSE);
        FL.setDirection(DcMotorSimple.Direction.FORWARD);
        RL.setDirection(DcMotorSimple.Direction.FORWARD);

    }

}
