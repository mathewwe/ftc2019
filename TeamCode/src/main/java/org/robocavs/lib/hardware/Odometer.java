package org.robocavs.lib.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

public class Odometer {
    public DcMotor x, y;
    private double WHEEL_SIZE;
    private double CPR;
    public enum Axis{X, Y, ALL}

    public Odometer(double wheelSizeIn, double countsPerRev){
        WHEEL_SIZE = wheelSizeIn;
        CPR = countsPerRev;
    }
    public double EncoderTicksPerIn(){
        return CPR/(WHEEL_SIZE * Math.PI);
    }
    public void reset(Axis axis){
        switch (axis){
            case X:
                x.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                x.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                break;
            case Y:
                y.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                y.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                break;
            case ALL:
                x.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                y.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                x.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                y.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
                break;
        }
    }
    public void init(HardwareMap hwMap){
        x = hwMap.dcMotor.get("X");
        y = hwMap.dcMotor.get("Y");
    }
    public double getDistanceX(){
        return -((double)x.getCurrentPosition())/EncoderTicksPerIn();
    }
    public double getDistanceY(){
        return -((double)y.getCurrentPosition())/EncoderTicksPerIn();
    }
    public double getCurrPosX(){
        return -(double)x.getCurrentPosition();
    }
    public double getCurrPosY(){
        return -(double)y.getCurrentPosition();
    }
}
