package org.robocavs.lib.hardware;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

public class TankDrivetrain extends Drivetrain{

    //public DcMotor FL, FR;

    private MotorType motorType;
    private double WHEEL_SIZE;
    private double GEAR_RATIO;

    public TankDrivetrain(double wheelSizeIn, double gearRatio, MotorType motorType){
        this.motorType = motorType;
        GEAR_RATIO = gearRatio;
        WHEEL_SIZE = wheelSizeIn;
    }
    @Override
    public double EncoderTicksPerIn(){
        double output;
        switch (motorType){
            case ORBITAL_20:
                output = (537.6/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case N20:
                output = (560/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case N40:
                output = (1120/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case N60:
                output = (1680/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case TORQUENADO:
                output = (1440/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            case E4T:
                output = (1440/GEAR_RATIO/(WHEEL_SIZE * Math.PI));
                break;
            default:
                output = 0;
                break;
        }
        return output;
    }
    @Override
    public void setMotorModes(DcMotor.RunMode runMode){
        FL.setMode(runMode);
        FR.setMode(runMode);
    }
    @Override
    public void setMotorPowers(double power){
        FL.setPower(power);
        FR.setPower(power);
    }
    @Override
    public void init(HardwareMap hwMap){
        FL = hwMap.dcMotor.get("FL");
        FR = hwMap.dcMotor.get("FR");

        FL.setDirection(DcMotorSimple.Direction.REVERSE);
        FR.setDirection(DcMotorSimple.Direction.FORWARD);

    }

}
