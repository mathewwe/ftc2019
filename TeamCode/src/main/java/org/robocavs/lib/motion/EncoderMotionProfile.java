package org.robocavs.lib.motion;

public class EncoderMotionProfile { // TODO: 7/29/2018 Power applied vs. velocity for actual robot would be good, can set target velocity/acceleration

    public EncoderMotionProfile(){

    }
    private int rampingTicksM;
    public double trapezoidalMP(double startPower, double endPower, double maxPower, int currentPos, int targetPos, int rampingTicks){
        double realStartMaxPower = maxPower - startPower;
        double realEndMaxPower = maxPower - endPower;
        double currentPosD = (double)currentPos;
        double targetPosD = (double)targetPos;
        double rampingTicksD;
        if(rampingTicks*2 < targetPos){
            rampingTicksM = rampingTicks;
        }
        else{
            rampingTicksM = (int)(((double)targetPos)/2);
        }
        rampingTicksD = (double)rampingTicksM;

        if(currentPos <= rampingTicksM){
            return realStartMaxPower*(currentPosD/rampingTicksD)+startPower;
        }
        else if(currentPos > rampingTicksM && currentPos <= (targetPos - rampingTicksM)){
            return maxPower;
        }
        else if(currentPos > (targetPos - rampingTicksM) && currentPos < targetPos){
            return realEndMaxPower*((targetPosD-currentPosD)/rampingTicksD)+endPower;
        }
        else return 0;

    }
    public int getRampingTicksM(){
        return rampingTicksM;
    }
}
