package org.robocavs.lib.motion;


import com.qualcomm.robotcore.util.Range;

public class GenericController {
    public GenericController(){ // TODO: 7/27/2018 Fix Integral action by adding anti-windup algorithm

    }
    private double iterationStart = 0;
    private double proportional;
    private double integral = 0;
    private double derivative;
    private double error;
    private double priorError = 0;
    private double output = 0;
    private double iterationTime;
    public double adjustAngle(double angle) {
        if (angle > 180) angle -= 360;
        if (angle <= -180) angle += 360;
        return angle;
    }
    public double controllerOutput(double setPoint, double currentPos, double maxVal, double kP, double kI, double kD){

        error = setPoint-currentPos;
        double iterationTime = (System.currentTimeMillis() - iterationStart)/1000;
        proportional = error;
        integral = integral + (error * iterationTime);
        derivative = (error - priorError)/iterationTime;

        output = kP*proportional + kI*integral + kD*derivative;

        iterationStart = System.currentTimeMillis();
        return Range.clip(output, -maxVal, maxVal);

    }
    public double gyroControllerOutput(double setPoint, double currentPos, double maxVal, double kP, double kI, double kD){

        error = adjustAngle(setPoint-currentPos);
        iterationTime = (System.currentTimeMillis() - iterationStart)/1000;
        proportional = error;
        integral = integral + (error * iterationTime);
        derivative = (error - priorError)/iterationTime;

        output = kP*proportional + kI*integral + kD*derivative;

        iterationStart = System.currentTimeMillis();
        return Range.clip(output, -maxVal, maxVal);

    }
    public double getProportional(){
        return proportional;
    }
    public double getIntegral(){
        return integral;
    }
    public double getDerivative(){
        return derivative;
    }
    public double getIterationTime(){
        return iterationTime;
    }
    public void resetController(){
        proportional = 0;
        integral = 0;
        derivative = 0;
        priorError = 0;
        iterationStart = System.currentTimeMillis();
    }
}
