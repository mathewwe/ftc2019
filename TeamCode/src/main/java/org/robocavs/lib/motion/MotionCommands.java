package org.robocavs.lib.motion;


import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import net.frogbots.ftcopmodetunercommon.opmode.TunableLinearOpMode;

import org.robocavs.lib.hardware.BoschIMU;
import org.robocavs.lib.hardware.Drivetrain;

public class MotionCommands { // TODO: 8/24/2018 Add angle setpoint parameter to methods that try to keep straight instead of always having it set to 0. Maybe use Math.round instead of casting everything
    private final static double turnP = .012;
    private final static double turnI = 0;
    private final static double turnD = 0;

    private final static double maintainHeadingP = .0137;
    private final static double maintainHeadingI = 0;
    private final static double maintainHeadingD = 0;

    private final static double driveP = 0;
    private final static double driveI = 0;
    private final static double driveD = 0;
    LinearOpMode op;
    public enum DrivetrainType{TANK, MECANUM}
    DrivetrainType drivetrainType;
    Drivetrain dt;
    EncoderMotionProfile MP = new EncoderMotionProfile();
    GenericController PID = new GenericController();

    public MotionCommands(LinearOpMode op, Drivetrain dt, DrivetrainType drivetrainType){
        this.op = op;
        this.dt = dt;
        this.drivetrainType = drivetrainType;
    }
    public MotionCommands(TunableLinearOpMode op, Drivetrain dt, DrivetrainType drivetrainType){
        this.op = op;
        this.dt = dt;
        this.drivetrainType = drivetrainType;
    }

    public void init(){
        switch (drivetrainType){
            case TANK:
                dt.init(op.hardwareMap);
                break;
            case MECANUM:
                dt.init(op.hardwareMap);
                break;
            default:
                break;
        }
    }

    public enum Direction{FORWARD, BACKWARD, LEFT, RIGHT}
    public void drive(double maxPower, double distanceIn, BoschIMU imu, Direction direction){
        switch (direction){
            case FORWARD:
                driveForward(maxPower, distanceIn, imu);
                break;
            case BACKWARD:
                driveBackward(maxPower, distanceIn, imu);
                break;
            case LEFT:
                driveLeft(maxPower, distanceIn, imu);
                break;
            case RIGHT:
                driveRight(maxPower, distanceIn, imu);
                break;
        }
    }
    private void template(){
        switch (drivetrainType){
            case MECANUM:
                break;
            case TANK:
                break;
        }
    }
    public void driveForward(double maxPower, double distanceIn, BoschIMU imu){
        double P, I, D;
        P = maintainHeadingP;
        I = maintainHeadingI;
        D = maintainHeadingD;
        PID.resetController();
        switch (drivetrainType){
            case MECANUM:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (dt.FL.getCurrentPosition() < (int)distanceIn*dt.EncoderTicksPerIn() && op.opModeIsActive()){
                    imu.loop();
                    double power = MP.trapezoidalMP(.1, .02, maxPower , dt.FL.getCurrentPosition(), (int)(distanceIn*dt.EncoderTicksPerIn()), 1500);
                    double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, P, I, D);
                    dt.FL.setPower(Range.clip(power - turnPower, -1, 1));
                    dt.FR.setPower(Range.clip(power + turnPower, -1, 1));
                    dt.RL.setPower(Range.clip(power - turnPower, -1, 1));
                    dt.RR.setPower(Range.clip(power + turnPower, -1, 1));
                    op.telemetry.addData("currpos", dt.FL.getCurrentPosition());
                    op.telemetry.addData("target", (int)(distanceIn*dt.EncoderTicksPerIn()));
                    op.telemetry.update();
                }
                dt.setMotorPowers(0);
                break;
            case TANK:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (dt.FL.getCurrentPosition() < (int)distanceIn*dt.EncoderTicksPerIn() && op.opModeIsActive()){
                    imu.loop();
                    double power = MP.trapezoidalMP(.1, .02, maxPower , Math.abs(dt.FL.getCurrentPosition()), (int)(distanceIn*dt.EncoderTicksPerIn()), 1500);
                    double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, P, I, D);
                    dt.FL.setPower(Range.clip(power - turnPower, -1, 1));
                    dt.FR.setPower(Range.clip(power + turnPower, -1, 1));
                    op.telemetry.addData("currpos", dt.FL.getCurrentPosition());
                    op.telemetry.addData("target", distanceIn*dt.EncoderTicksPerIn());
                    op.telemetry.update();
                }
                dt.setMotorPowers(0);
                break;
            default:
                break;
        }
    }
    public void driveBackward(double maxPower, double distanceIn, BoschIMU imu){
        double P, I, D;
        P = maintainHeadingP;
        I = maintainHeadingI;
        D = maintainHeadingD;
        PID.resetController();
        switch (drivetrainType){
            case MECANUM:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (Math.abs(dt.FL.getCurrentPosition()) < (int)distanceIn*dt.EncoderTicksPerIn() && op.opModeIsActive()){
                    imu.loop();
                    double power = MP.trapezoidalMP(.1, .02, maxPower , Math.abs(dt.FL.getCurrentPosition()), (int)(distanceIn*dt.EncoderTicksPerIn()), 1500);
                    double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, P, I, D);
                    dt.FL.setPower(Range.clip(-power - turnPower, -1, 1));
                    dt.FR.setPower(Range.clip(-power + turnPower, -1, 1));
                    dt.RL.setPower(Range.clip(-power - turnPower, -1, 1));
                    dt.RR.setPower(Range.clip(-power + turnPower, -1, 1));
                    op.telemetry.addData("currpos", dt.FL.getCurrentPosition());
                    op.telemetry.addData("target", distanceIn*dt.EncoderTicksPerIn());
                    op.telemetry.update();
                }
                dt.setMotorPowers(0);
                break;
            case TANK:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (Math.abs(dt.FL.getCurrentPosition()) < (int)distanceIn*dt.EncoderTicksPerIn() && op.opModeIsActive()){
                    imu.loop();
                    double power = MP.trapezoidalMP(.1, .02, maxPower , Math.abs(dt.FL.getCurrentPosition()), (int)(distanceIn*dt.EncoderTicksPerIn()), 1500);
                    double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, P, I, D);
                    dt.FL.setPower(Range.clip(-power - turnPower, -1, 1));
                    dt.FR.setPower(Range.clip(-power + turnPower, -1, 1));
                    op.telemetry.addData("currpos", dt.FL.getCurrentPosition());
                    op.telemetry.addData("target", distanceIn*dt.EncoderTicksPerIn());
                    op.telemetry.update();
                }
                dt.setMotorPowers(0);
                break;
        }
    }
    public void driveLeft(double maxPower, double distanceIn, BoschIMU imu){ //Not even close to accurate, don't use for accurate distance travelling
        double P, I, D;
        P = maintainHeadingP;
        I = maintainHeadingI;
        D = maintainHeadingD;
        PID.resetController();
        switch (drivetrainType){
            case MECANUM:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (Math.abs(dt.FL.getCurrentPosition()) < (int)distanceIn*dt.EncoderTicksPerIn() && op.opModeIsActive()){
                    imu.loop();
                    double power = MP.trapezoidalMP(.1, .02, maxPower , Math.abs(dt.FL.getCurrentPosition()), (int)(distanceIn*dt.EncoderTicksPerIn()), 1500);
                    double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, P, I, D);
                    dt.FL.setPower(Range.clip(-power - turnPower, -1, 1));
                    dt.FR.setPower(Range.clip(power + turnPower, -1, 1));
                    dt.RL.setPower(Range.clip(power - turnPower, -1, 1));
                    dt.RR.setPower(Range.clip(-power + turnPower, -1, 1));
                    op.telemetry.addData("currpos", dt.FL.getCurrentPosition());
                    op.telemetry.addData("target", distanceIn*dt.EncoderTicksPerIn());
                    op.telemetry.update();
                }
                dt.setMotorPowers(0);
                break;
            case TANK:
                while (op.opModeIsActive()){
                    op.telemetry.addData("Error", "Tank drive doesn't go sideways dumbo");
                    op.telemetry.update();
                }
                break;
        }
    }
    public void driveRight(double maxPower, double distanceIn, BoschIMU imu){ //Not even close to accurate, don't use for accurate distance travelling
        double P, I, D;
        P = maintainHeadingP;
        I = maintainHeadingI;
        D = maintainHeadingD;
        PID.resetController();
        switch (drivetrainType){
            case MECANUM:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (dt.FL.getCurrentPosition() < (int)distanceIn*dt.EncoderTicksPerIn() && op.opModeIsActive()){
                    imu.loop();
                    double power = MP.trapezoidalMP(.1, .02, maxPower , Math.abs(dt.FL.getCurrentPosition()), (int)(distanceIn*dt.EncoderTicksPerIn()), 1500);
                    double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, P, I, D);
                    dt.FL.setPower(Range.clip(power - turnPower, -1, 1));
                    dt.FR.setPower(Range.clip(-power + turnPower, -1, 1));
                    dt.RL.setPower(Range.clip(-power - turnPower, -1, 1));
                    dt.RR.setPower(Range.clip(power + turnPower, -1, 1));
                    op.telemetry.addData("currpos", dt.FL.getCurrentPosition());
                    op.telemetry.addData("target", distanceIn*dt.EncoderTicksPerIn());
                    op.telemetry.update();
                }
                dt.setMotorPowers(0);
                break;
            case TANK:
                while (op.opModeIsActive()){
                    op.telemetry.addData("Error", "Tank drive doesn't go sideways dumbo");
                    op.telemetry.update();
                }
                break;
        }
    }
    public void turnToHeading(double maxPower, double heading, double tolerance, BoschIMU imu){
        double P, I, D;
        P = turnP;
        I = turnI;
        D = turnD;
        PID.resetController();
        double start = System.currentTimeMillis();
        op.telemetry.addData("Heading", imu.heading);
        op.telemetry.addData("Target", heading);
        switch (drivetrainType){
            case MECANUM:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (op.opModeIsActive()){
                    imu.loop();
                    op.telemetry.addData("Heading", imu.heading);
                    op.telemetry.addData("Target", heading);
                    op.telemetry.update();
                    double pidOut = PID.gyroControllerOutput(heading, imu.heading, maxPower, P, I, D);
                    dt.FL.setPower(-pidOut);
                    dt.FR.setPower(pidOut);
                    dt.RL.setPower(-pidOut);
                    dt.RR.setPower(pidOut);
                    if(Math.abs(PID.adjustAngle(heading - imu.heading)) < tolerance && System.currentTimeMillis()-start >=750){
                        break;
                    }
                    else if (Math.abs(PID.adjustAngle(heading - imu.heading)) < tolerance);
                    else {
                        start = System.currentTimeMillis();
                    }
                }
                dt.setMotorPowers(0);
                op.telemetry.clearAll();
                break;
            case TANK:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (op.opModeIsActive()){
                    imu.loop();
                    op.telemetry.addData("Heading", imu.heading);
                    op.telemetry.addData("Target", heading);
                    op.telemetry.update();
                    double pidOut = PID.gyroControllerOutput(heading, imu.heading, maxPower, P, I, D);
                    dt.FL.setPower(-pidOut);
                    dt.FR.setPower(pidOut);
                    if(Math.abs(PID.adjustAngle(heading - imu.heading)) < tolerance && System.currentTimeMillis()-start >=750){
                        break;
                    }
                    else if (Math.abs(PID.adjustAngle(heading - imu.heading)) < tolerance);
                    else {
                        start = System.currentTimeMillis();
                    }
                }
                dt.setMotorPowers(0);
                op.telemetry.clearAll();
                break;
        }
    }
    public void driveByAngle(double maxPower, double distance, double angleDeg, BoschIMU imu){ //Distances for this mean basically nothing, you're gonna have to use sensors for this bad boy
        double P, I, D;
        P = maintainHeadingP;
        I = maintainHeadingI;
        D = maintainHeadingD;
        PID.resetController();
        switch (drivetrainType){
            case MECANUM:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (dt.FL.getCurrentPosition() <= distance*dt.EncoderTicksPerIn()){
                    double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, P, I, D);
                    double power = MP.trapezoidalMP(.2, .02, maxPower , dt.FL.getCurrentPosition(), (int)(distance*dt.EncoderTicksPerIn()), 1000);
                    dt.FL.setPower(Range.clip(power*Math.cos(angleDeg*(Math.PI/180)-(Math.PI/4)) - turnPower, -1, 1));
                    dt.FR.setPower(Range.clip(power*Math.sin(angleDeg*(Math.PI/180)-(Math.PI/4)) + turnPower, -1, 1));
                    dt.RL.setPower(Range.clip(power*Math.sin(angleDeg*(Math.PI/180)-(Math.PI/4)) - turnPower, -1, 1));
                    dt.RR.setPower(Range.clip(power*Math.cos(angleDeg*(Math.PI/180)-(Math.PI/4)) + turnPower, -1, 1));
                    op.telemetry.addData("Power", power);
                    op.telemetry.addData("Current Position", dt.FL.getCurrentPosition());
                    op.telemetry.addData("Target Position", distance*dt.EncoderTicksPerIn());
                    op.telemetry.addData("Heading", imu.heading);
                    op.telemetry.update();
                }
                dt.setMotorPowers(0);
                break;
            case TANK:
                while (op.opModeIsActive()){
                    op.telemetry.addData("Error", "Tank drive doesn't go sideways dumbo");
                    op.telemetry.update();
                }
                break;
        }
    }
}
