package org.robocavs.lib.motion;


import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import net.frogbots.ftcopmodetunercommon.opmode.TunableLinearOpMode;

import org.robocavs.lib.hardware.BoschIMU;
import org.robocavs.lib.hardware.Drivetrain;
import org.robocavs.lib.hardware.Odometer;

public class MotionCommandsOdometer { // TODO: 8/24/2018 Add angle setpoint parameter to methods that try to keep straight instead of always having it set to 0. Maybe use Math.round instead of casting everything
    private final static double turnP = .012;
    private final static double turnI = 0;
    private final static double turnD = 0;

    private final static double maintainHeadingP = .0137;
    private final static double maintainHeadingI = 0;
    private final static double maintainHeadingD = 0;

    private final static double driveP = 0;
    private final static double driveI = 0;
    private final static double driveD = 0;
    LinearOpMode op;
    public enum DrivetrainType{TANK, MECANUM}
    DrivetrainType drivetrainType;
    Drivetrain dt;
    EncoderMotionProfile MP = new EncoderMotionProfile();
    GenericController PID = new GenericController();
    GenericController PID1 = new GenericController();
    GenericController PID2 = new GenericController();
    public Odometer odometer = new Odometer(5.95/2.54, 1440);

    public MotionCommandsOdometer(LinearOpMode op, Drivetrain dt, DrivetrainType drivetrainType){
        this.op = op;
        this.dt = dt;
        this.drivetrainType = drivetrainType;
    }
    public MotionCommandsOdometer(TunableLinearOpMode op, Drivetrain dt, DrivetrainType drivetrainType){
        this.op = op;
        this.dt = dt;
        this.drivetrainType = drivetrainType;
    }

    public void init(){
        switch (drivetrainType){
            case TANK:
                dt.init(op.hardwareMap);
                odometer.init(op.hardwareMap);
                break;
            case MECANUM:
                dt.init(op.hardwareMap);
                odometer.init(op.hardwareMap);
                break;
            default:
                break;
        }
    }
    private void template(){
        switch (drivetrainType){
            case MECANUM:
                break;
            case TANK:
                break;
        }
    }
    public void drive(double maxPower, double x, double y, double heading, BoschIMU imu){
        double start;
        switch (drivetrainType){
            case MECANUM:
                PID.resetController();
                PID1.resetController();
                PID2.resetController();
                start = System.currentTimeMillis();
                while((Math.abs((odometer.EncoderTicksPerIn()*y)-odometer.getCurrPosY()) > 20 || Math.abs((odometer.EncoderTicksPerIn()*x)-odometer.getCurrPosX()) > 20 || Math.abs(heading-imu.heading) > 0.2 || System.currentTimeMillis() - start < 750) && op.opModeIsActive()){
                    imu.loop();
                    double drivePower = PID.controllerOutput(odometer.EncoderTicksPerIn()*y, odometer.getCurrPosY(), maxPower, .0006, 0, 0);
                    double turnPower = PID1.gyroControllerOutput(heading, imu.heading, 1, .0137, 0, 0);
                    double strafePower = PID2.controllerOutput(odometer.EncoderTicksPerIn()*x, odometer.getCurrPosX(), maxPower, .0008, 0, 0);
                    dt.FL.setPower(Range.clip(drivePower+strafePower-turnPower, -1, 1)); //+strafePower-turnPower
                    dt.FR.setPower(Range.clip(drivePower-strafePower+turnPower, -1, 1)); //-strafePower+turnPower
                    dt.RL.setPower(Range.clip(drivePower-strafePower-turnPower, -1, 1)); //-strafePower-turnPower
                    dt.RR.setPower(Range.clip(drivePower+strafePower+turnPower, -1, 1)); //+strafePower+turnPower

                    op.telemetry.addData("CurrPosY", odometer.getDistanceY());
                    op.telemetry.addData("CurrPosX", odometer.getDistanceX());
                    op.telemetry.addData("Heading", imu.heading);
                    op.telemetry.update();
                    if(Math.abs(PID.adjustAngle(heading - imu.heading)) > .2 || Math.abs((odometer.EncoderTicksPerIn()*y)-odometer.getCurrPosY()) > 20 || Math.abs((odometer.EncoderTicksPerIn()*x)-odometer.getCurrPosX()) > 20){
                        start = System.currentTimeMillis();
                    }
                }
                break;
            case TANK:
                PID.resetController();
                PID1.resetController();
                start = System.currentTimeMillis();
                while((Math.abs((odometer.EncoderTicksPerIn()*y)-odometer.getCurrPosY()) > 20 || Math.abs(heading-imu.heading) > 0.2 || System.currentTimeMillis() - start < 750) && op.opModeIsActive()){
                    imu.loop();
                    double drivePower = PID.controllerOutput(odometer.EncoderTicksPerIn()*y, odometer.getCurrPosY(), maxPower, .0006, 0, 0);
                    double turnPower = PID1.gyroControllerOutput(heading, imu.heading, 1, .0137, 0, 0);
                    dt.FL.setPower(Range.clip(drivePower-turnPower, -1, 1)); //+strafePower-turnPower
                    dt.FR.setPower(Range.clip(drivePower+turnPower, -1, 1)); //-strafePower+turnPower

                    op.telemetry.addData("CurrPosY", odometer.getDistanceY());
                    op.telemetry.addData("CurrPosX", odometer.getDistanceX());
                    op.telemetry.addData("Heading", imu.heading);
                    op.telemetry.update();
                    if(Math.abs(PID.adjustAngle(heading - imu.heading)) > .2 || Math.abs((odometer.EncoderTicksPerIn()*y)-odometer.getCurrPosY()) > 20){
                        start = System.currentTimeMillis();
                    }
                }
                break;
        }
    }

    public void turnToHeading(double maxPower, double heading, double tolerance, BoschIMU imu){
        double P, I, D;
        P = turnP;
        I = turnI;
        D = turnD;
        PID.resetController();
        double start = System.currentTimeMillis();
        op.telemetry.addData("Heading", imu.heading);
        op.telemetry.addData("Target", heading);
        switch (drivetrainType){
            case MECANUM:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (op.opModeIsActive()){
                    imu.loop();
                    op.telemetry.addData("Heading", imu.heading);
                    op.telemetry.addData("Target", heading);
                    op.telemetry.update();
                    double pidOut = PID.gyroControllerOutput(heading, imu.heading, maxPower, P, I, D);
                    dt.FL.setPower(-pidOut);
                    dt.FR.setPower(pidOut);
                    dt.RL.setPower(-pidOut);
                    dt.RR.setPower(pidOut);
                    if(Math.abs(PID.adjustAngle(heading - imu.heading)) < tolerance && System.currentTimeMillis()-start >=750){
                        break;
                    }
                    else if (Math.abs(PID.adjustAngle(heading - imu.heading)) < tolerance);
                    else {
                        start = System.currentTimeMillis();
                    }
                }
                dt.setMotorPowers(0);
                op.telemetry.clearAll();
                break;
            case TANK:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (op.opModeIsActive()){
                    imu.loop();
                    op.telemetry.addData("Heading", imu.heading);
                    op.telemetry.addData("Target", heading);
                    op.telemetry.update();
                    double pidOut = PID.gyroControllerOutput(heading, imu.heading, maxPower, P, I, D);
                    dt.FL.setPower(-pidOut);
                    dt.FR.setPower(pidOut);
                    if(Math.abs(PID.adjustAngle(heading - imu.heading)) < tolerance && System.currentTimeMillis()-start >=750){
                        break;
                    }
                    else if (Math.abs(PID.adjustAngle(heading - imu.heading)) < tolerance);
                    else {
                        start = System.currentTimeMillis();
                    }
                }
                dt.setMotorPowers(0);
                op.telemetry.clearAll();
                break;
        }
    }
    public void driveByAngle(double maxPower, double distance, double angleDeg, BoschIMU imu){ //Must use odometry encoders
        double P, I, D;
        P = maintainHeadingP;
        I = maintainHeadingI;
        D = maintainHeadingD;
        PID.resetController();
        int step = 1;
        double xDist = Math.abs(distance * Math.cos(Math.toRadians(angleDeg)));
        double yDist = Math.abs(distance * Math.sin(Math.toRadians(angleDeg)));
        double xEncoderPlaceholder = 1/dt.EncoderTicksPerIn();
        double yEncoderPlaceholder = 1/dt.EncoderTicksPerIn();
        switch (drivetrainType){
            case MECANUM:
                dt.setMotorModes(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                dt.setMotorModes(DcMotor.RunMode.RUN_USING_ENCODER);
                while (Math.hypot(xEncoderPlaceholder, yEncoderPlaceholder) <= distance*dt.EncoderTicksPerIn()){
                    double turnPower = PID.gyroControllerOutput(0, imu.heading, 1, P, I, D);
                    if(Math.abs(xEncoderPlaceholder) >= xDist || Math.abs(yEncoderPlaceholder) >= yDist){
                        switch (step){
                            case 1:
                                if(angleDeg >= 90 && angleDeg < 270){
                                    dt.FL.setPower(Range.clip(-.1 - turnPower, -1, 1));
                                    dt.FR.setPower(Range.clip(.1 + turnPower, -1, 1));
                                    dt.RL.setPower(Range.clip(.1 - turnPower, -1, 1));
                                    dt.RR.setPower(Range.clip(-.1 + turnPower, -1, 1));
                                    if (Math.abs(xEncoderPlaceholder) >= xDist){
                                        step++;
                                    }
                                }
                                else{
                                    dt.FL.setPower(Range.clip(.1 - turnPower, -1, 1));
                                    dt.FR.setPower(Range.clip(-.1 + turnPower, -1, 1));
                                    dt.RL.setPower(Range.clip(-.1 - turnPower, -1, 1));
                                    dt.RR.setPower(Range.clip(.1 + turnPower, -1, 1));
                                    if(Math.abs(xEncoderPlaceholder) >= xDist){
                                        step++;
                                    }
                                }
                                break;
                            case 2:
                                if(angleDeg >= 0 && angleDeg < 180){
                                    dt.FL.setPower(Range.clip(.1 - turnPower, -1, 1));
                                    dt.FR.setPower(Range.clip(.1 + turnPower, -1, 1));
                                    dt.RL.setPower(Range.clip(.1 - turnPower, -1, 1));
                                    dt.RR.setPower(Range.clip(.1 + turnPower, -1, 1));
                                    if (Math.abs(yEncoderPlaceholder) >= yDist){
                                        step++;
                                    }
                                }
                                else{
                                    dt.FL.setPower(Range.clip(-.1 - turnPower, -1, 1));
                                    dt.FR.setPower(Range.clip(-.1 + turnPower, -1, 1));
                                    dt.RL.setPower(Range.clip(-.1 - turnPower, -1, 1));
                                    dt.RR.setPower(Range.clip(-.1 + turnPower, -1, 1));
                                    if(Math.abs(yEncoderPlaceholder) >= yDist){
                                        step++;
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else {
                        double power = MP.trapezoidalMP(.2, .02, maxPower , (int) Math.hypot(xEncoderPlaceholder, yEncoderPlaceholder), (int)(distance*dt.EncoderTicksPerIn()), 1000);
                        dt.FL.setPower(Range.clip(power*Math.cos(angleDeg*(Math.PI/180)-(Math.PI/4)) - turnPower, -1, 1));
                        dt.FR.setPower(Range.clip(power*Math.sin(angleDeg*(Math.PI/180)-(Math.PI/4)) + turnPower, -1, 1));
                        dt.RL.setPower(Range.clip(power*Math.sin(angleDeg*(Math.PI/180)-(Math.PI/4)) - turnPower, -1, 1));
                        dt.RR.setPower(Range.clip(power*Math.cos(angleDeg*(Math.PI/180)-(Math.PI/4)) + turnPower, -1, 1));
                        op.telemetry.addData("Power", power);
                        op.telemetry.addData("Current Position", dt.FL.getCurrentPosition());
                        op.telemetry.addData("Target Position", distance*dt.EncoderTicksPerIn());
                        op.telemetry.addData("Heading", imu.heading);
                        op.telemetry.addData("X Target", xDist);
                        op.telemetry.addData("Y Target", yDist);
                        op.telemetry.update();
                    }
                }
                dt.setMotorPowers(0);
                break;
            case TANK:
                while (op.opModeIsActive()){
                    op.telemetry.addData("Error", "Tank drive doesn't go sideways dumbo");
                    op.telemetry.update();
                }
                break;
        }
    }
}
