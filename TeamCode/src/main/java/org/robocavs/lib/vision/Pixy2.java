package org.robocavs.lib.vision;


import com.qualcomm.robotcore.hardware.I2cAddr;
import com.qualcomm.robotcore.hardware.I2cDeviceSynch;
import com.qualcomm.robotcore.hardware.I2cDeviceSynchDevice;

public class Pixy2 extends I2cDeviceSynchDevice<I2cDeviceSynch> {


    @Override
    protected boolean doInitialize() {
        return true;
    }

    @Override
    public Manufacturer getManufacturer() {
        return Manufacturer.valueOf("Charmed Labs");
    }

    @Override
    public String getDeviceName() {
        return "Pixy2";
    }

    public Pixy2(I2cDeviceSynch deviceClient){
        super(deviceClient, true);

        this.deviceClient.setI2cAddress(I2cAddr.create7bit(0x54));

        super.registerArmingStateCallback(false);
        this.deviceClient.engage();
    }
    public void write(byte[] value){
        deviceClient.write(0, value);
    }
    public byte[] read(){
        return deviceClient.read(0, 22);
    }
}
