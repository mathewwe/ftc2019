package org.robocavs.lib.vision;

import com.vuforia.HINT;
import com.vuforia.Vuforia;

import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;
import org.firstinspires.ftc.teamcode.R;

/**
 * Created by mathe_000 on 12/24/2016.
 */
public class VuforiaCV {

    public ClosableVuforiaLocalizer.Parameters param = new VuforiaLocalizer.Parameters(R.id.cameraMonitorViewId);
    public ClosableVuforiaLocalizer vuforia;
    public VuforiaTrackables RelicTrackables;
    public VuforiaTrackable RelicTemplate;
    public RelicRecoveryVuMark vuMark;
    public VuforiaCV(){

    }
    public void close(){
        vuforia.close();
    }
    public float getPose(final VuforiaTrackable obj, int index){
        OpenGLMatrix pose;
        VectorF translation;
        pose = ((VuforiaTrackableDefaultListener) obj.getListener()).getPose();
        if(pose != null) {
            translation = pose.getTranslation();
            return translation.get(index);
        }
        else{
            return 0; //MAKE SURE TO COMPENSATE.
        }
    }
    public double targetDistance(VuforiaTrackable obj){
        OpenGLMatrix pose;
        VectorF translation;
        double distance;
        pose = ((VuforiaTrackableDefaultListener) obj.getListener()).getPose();
        if(pose != null) {
            translation = pose.getTranslation();
            distance = Math.sqrt(translation.get(2) * translation.get(2) + translation.get(0) * translation.get(0));  // Pythagoras calc of hypotenuse
            return distance;
        }
        else{
            return 0; //MAKE SURE TO COMPENSATE.
        }
    }
    public double targetAngle(VuforiaTrackable obj){
        OpenGLMatrix pose;
        VectorF translation;
        double angle;
        pose = ((VuforiaTrackableDefaultListener) obj.getListener()).getPose();
        if(pose != null) {
            translation = pose.getTranslation();
            angle = Math.atan2(translation.get(2), translation.get(0)); // in radians
            return Math.toDegrees(angle);
        }
        else{
            return 0; //MAKE SURE TO COMPENSATE.
        }
    }
    public OpenGLMatrix rawPose(VuforiaTrackable obj){
        return ((VuforiaTrackableDefaultListener) obj.getListener()).getPose();
    }

    public void init(){
        param.cameraDirection = VuforiaLocalizer.CameraDirection.FRONT;
        param.vuforiaLicenseKey = "AcLL+AD/////AAAAGbjvuBpXskZYpWZYqpbntgFnVpdzgL/GCP21WABGLdYlHgkqlzsJQAiWS4zL6jxCebLU8fQdTA3WpFPxeVbl9OPFR2qcAHtHFII0ySxGjS2/0lqttS7mCiES2r2VkyalfQOcWmePNZamwmraJs/QqfAfkcW7TJsesMo/KYkHO3idc6xi6K0ZqsiC97kMps7ibmpAMQXr2I8VzdhO5250kna5dmzr5AGFzxjPSWb5lM/fckBXQZhFxt6AnBTBg9FV9iL1HwJm+t18xS4zuQTLXpZ5lALzZ5jKmxSPEt7rlwW2zrcscbM2TyzhOQXFYFvn083HJmIK2wXQqJHyJw89r5FSUOhVALUwtJjHuEib1QJv";
        param.cameraMonitorFeedback = VuforiaLocalizer.Parameters.CameraMonitorFeedback.AXES;
        Vuforia.setHint(HINT.HINT_MAX_SIMULTANEOUS_IMAGE_TARGETS, 4);
        vuforia = new ClosableVuforiaLocalizer(param);
        RelicTrackables = vuforia.loadTrackablesFromAsset("RelicVuMark");
        RelicTemplate = RelicTrackables.get(0);
        RelicTrackables.get(0).setName("Relic VuMark");
    }
    public void start(){
        RelicTrackables.activate();
    }
    public void loop(){
        vuMark = RelicRecoveryVuMark.from(RelicTemplate);
    }
}
